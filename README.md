# consoenergie

Ce projet a pour objectif de suivre la consomation d'energie familiale pour limiter nos rejets de gaz à effet de serre.

## Technos utilisées

* Python
* ArangoDb
* VueJs
* Docker

## Getting started

## Installation

### Python

To use poetry, we need to install it : https://python-poetry.org/docs/ 

curl -sSL https://install.python-poetry.org | python3 -

To init the poetry project, 

poetry init

... and follow the process

This process permits to add "FastApi" as a development dependencies

### FastApi

https://www.younup.fr/blog/fastapi-nouveau-framework-web-api-python

Tout est dans le fichier consoenergie.py

Pour lancer le serveur, il faut taper la cmd suivate

```
uvicorn consoenergie:app --reload
```

Pour faire un appel à l'api ainsi démarrée

http://127.0.0.1:8000/

Ici, le tuto de base de fastApi

https://www.youtube.com/watch?v=alsWaMLEJd8

